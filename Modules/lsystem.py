"""@package lsystem @brief lsystem object.
@date 2021-01-3 Sun 01:01 PM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2021 Sangsoic
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import pathlib

class LSystem :

    ##
    # @brief Constructs object.
    # @author Sangsoic <sangsoic@protonmail.com>
    # @version 0.1
    # @date 2021-01-3 Sun 01:11 PM
    # @param axiom Attribute.
    # @param rules Attribute.
    # @param length Attribute.
    # @param angle Attribute.
    # @param level Attribute.
    def __init__(self, axiom, rules, length, angle, level) :
        self.axiom = axiom;
        self.rules = rules;
        self.length = length;
        self.angle = angle;
        self.level = level;

    ##
    # @brief Updates turtle encoding table.
    # @author Sangsoic <sangsoic@protonmail.com>
    # @version 0.1
    # @date 2021-01-3 Sun 01:19 PM
    # @param table Turtle encoding table.
    # @return Updates turtle encoding table.
    def update_table(self, table) :
        table = table.copy();
        table['a'] = table['a'] % self.length;
        table['b'] = table['b'] % self.length;
        table['+'] = table['+'] % self.angle;
        table['-'] = table['-'] % self.angle;
        return table;

    ##
    # @brief Converts given production l-system production rule into python procedure.
    # @author Sangsoic <sangsoic@protonmail.com>
    # @version 0.2
    # @date 2021-01-12 Tue 07:48 AM
    # @param symbol Symbol.
    # @param enctable Encoding table.
    # @return Python procedure.
    def transcode_rule(self, symbol, enctable) :
        procedure = "def rule_%d(level) :\n\tglobal s, h0, h1;\n" % ord(symbol);
        if symbol in self.rules.keys() :
            recursivecall = "";
            for c in self.rules[symbol] :
                recursivecall += "rule_%d(level-1);" % ord(c);
            basecall = "\telse :\n\t\t%s\n" % enctable[symbol] if symbol in enctable.keys() else "";
            procedure += "\tif level :\n\t\t%s\n%s\n" % (recursivecall, basecall);
        else :
            procedure += "\t%s\n\n" % enctable[symbol] if symbol in enctable.keys() else "\tpass;\n\n";
        return procedure;

    ##
    # @brief Converts given production l-system script into python turtle script.
    # @author Sangsoic <sangsoic@protonmail.com>
    # @version 0.2
    # @date 2021-01-12 Tue 07:25 AM
    # @param enctable Encoding table.
    # @return Python turtle script.
    def transcode(self, enctable) :
        script = """from turtle import *
from collections import *
from sys import *

level = %d;
if level >= getrecursionlimit() : setrecursionlimit(level+1);
speed(0);
color('black');
s = deque();
\n""" % (self.level);
        symbols = set(enctable.keys()) | set(self.rules.keys());
        for symbol in symbols:
            script += self.transcode_rule(symbol, enctable);
        recursivecall = "";
        for c in self.axiom :
            recursivecall += "rule_%d(level);" % ord(c);
        script += "def axiom(level) :\n\t%s\n\naxiom(level);\nexitonclick();" % (recursivecall);
        return script;

    ##
    # @brief Exports turtle instructions into a given file.
    # @author Sangsoic <sangsoic@protonmail.com>
    # @version 0.2
    # @date 2021-01-3 Sun 02:30 PM
    # @param pathout Output file path.
    # @param script Turtle script.
    def export(self, pathout, script) :
        if os.access(str(pathlib.Path(pathout).parent), os.W_OK) :
            with open(pathout, 'w') as fileout :
                fileout.write(script);
        else :
            raise_and_exit(ERROR_MSGS[10], os.EX_NOINPUT);
    
