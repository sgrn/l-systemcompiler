"""@package compiler
@brief Routines related to Lexing, Parsing, and Type Checking.
@date 2021-01-3 Sun 05:30 PM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2021 Sangsoic
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import re
import itertools

from Modules.constants import *
from Modules.io import *

##
# @brief Lexer.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 05:35 PM
# @param plain Input file content.
# @return Generator walking through input file words.
def lex(plain) :
    split = plain.split(maxsplit=1);
    head = split[0];
    if head:
        yield head;
        if len(split) == 2 :
            yield from lex(split[1]);

##
# @brief Parse production rules.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 05:55 PM
# @param tokens Generator of words.
# @param val Current production rule to be parsed.
# @return List of words contained in plain file.
def subparse(tokens, val) :
    def no_syntax_error(val) :
        return re.fullmatch(r'"[%s]=[%s]+"' % (KEYCHARS, KEYCHARS), val);
    def subparse(tokens) :
        backup, new = itertools.tee(tokens);
        parsed = {};
        tokens = backup;
        val = next(new, "");
        if no_syntax_error(val) :
            parsed, tokens = subparse(new);
            parsed.update(dict((val[1:-1].split('='),)));
        return parsed, tokens;
    if no_syntax_error(val) :
        parsed, tokens = subparse(tokens);
        parsed.update(dict((val[1:-1].split('='),)));
    else :
        raise_and_exit(ERROR_MSGS[6], os.EX_DATAERR);
    return parsed, tokens;

##
# @brief Checks attribute validity (axiom, rules, ...)
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 06:05 PM
# @param attr Current attribute.
# @param attrs List of remaining valid attributes.
def check_attribute(attr, attrs) :
    if attr in attrs :
        del attrs[attrs.index(attr)];
    else :
        raise_and_exit(ERROR_MSGS[2], os.EX_DATAERR);

##
# @brief Checks operator validity.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 06:08 PM
# @param op Current operator.
def check_operator(op) :
    if not op :
        raise_and_exit(ERROR_MSGS[3], os.EX_DATAERR);
    elif op != "=" :
        raise_and_exit(ERROR_MSGS[4], os.EX_DATAERR);

##
# @brief Checks type validity.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 06:19 PM
# @param op Current operator.
# @param attr Current attribute.
# @param val Current attribute value.
# @return Tuple containing parsed/transcoded value and updated token generator. 
def check_type(tokens, attr, val) :
    if not val :
        raise_and_exit(ERROR_MSGS[9], os.EX_DATAERR);
    if attr == KEYWORDS[1] :
        val, tokens = subparse(tokens, val);
    elif attr in KEYWORDS[2:5] :
        if re.fullmatch(r'^-?\d+(?:\.\d+)?$', val) :
            val = float(val);
        else :
            raise_and_exit(ERROR_MSGS[7], os.EX_DATAERR);
    else :
        if re.fullmatch(r'"[%s]+"' % KEYCHARS, val) :
            val = val[1:-1];
        else :
            raise_and_exit(ERROR_MSGS[8], os.EX_DATAERR);
    return val, tokens;

##
# @brief Detects syntax error and subparse.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 06:56 PM
# @param tokens Generator of words.
# @param attrs List of remaining valid attributes.
# @return Tuple containing parsed/transcoded attribute and updated token generator. 
def detect_syntax_error_and_subparse(tokens, attrs) :
    parsed = {};
    attr = next(tokens, False);
    if attr :
        check_attribute(attr, attrs);
        op = next(tokens, False);
        check_operator(op);
        val = next(tokens, False);
        val, tokens = check_type(tokens, attr, val);
        parsed[attr] = val;
    elif attrs == [KEYWORDS[3]] :
        del attrs[0];
    else :
        raise_and_exit(ERROR_MSGS[5], os.EX_DATAERR);
    return parsed, tokens;

##
# @brief Parses tokens.
# @author Sangsoic <sangsoic@protonmail.com>
# @version 0.1
# @date 2021-01-3 Sun 07:20 PM
# @param tokens Generator of words.
# @return Parsed tokens.
def parse(tokens) :
    def parse(tokens, attrs = list(KEYWORDS)) :
        parsed, tokens = detect_syntax_error_and_subparse(tokens, attrs);
        if attrs :
            parsed.update(parse(tokens));
        return parsed;
    parsed = {KEYWORDS[3] : 10};
    parsed.update(parse(tokens));
    return parsed;

